from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
import requests
from urllib.request import urlopen
import json
from datetime import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

class ActionSchedule(Action):

     def name(self) -> Text:
         return "action_schedule"

     def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

           
           print(tracker.get_slot("duree"))
           print(tracker.get_slot("date"))
           
           duree = tracker.get_slot("duree")
           date = tracker.get_slot("date")

           textVide = "Y a aucune salle disponible dans cette date"
           try:
              urlApi = "https://edt-api.univ-avignon.fr/app.php/api/salles/disponibilite?site=CERI&duree="+duree+"&debut=11&date="+date+""
              response = urlopen(urlApi)
              data_json = json.loads(response.read())
              dispatcher.utter_message(data_json["results"][0]["libelle"])
              print(data_json["results"][0]["libelle"])
           except:
              print(textVide)
              dispatcher.utter_message(textVide)

     
	
           return []


class ActionMaster(Action):

     def name(self) -> Text:
         return "action_teacher"

     def run(self, dispatcher: CollectingDispatcher,
             tracker: Tracker,
             domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

           
           print(tracker.get_slot("code"))
           print(tracker.get_slot("time"))
           
           code = tracker.get_slot("code")
           time = tracker.get_slot("time")

           
           
           urlApi = "https://edt-api.univ-avignon.fr/app.php/api/events_enseignant/"+code+""
           response = urlopen(urlApi)
           data_json = json.loads(response.read())

           filterData = data_json["results"]

           textAucun = "Vous avez aucun cours dans ce jour"
          

           x = list(filter(lambda x: time in x["start"],filterData))
           if len(x) > 0:
            dispatcher.utter_message(x[0]["title"][:40])
           else:
            print(textAucun)
            dispatcher.utter_message(textAucun)

           return []


class ActionSubmit(Action):
    def name(self) -> Text:
        return "action_submit"

    def run(
        self,
        dispatcher,
        tracker: Tracker,
        domain: "DomainDict",
    ) -> List[Dict[Text, Any]]:

        SendEmail(
            tracker.get_slot("email")+"@gmail.com",
            tracker.get_slot("subject"),
            tracker.get_slot("message")
        )
        print("-------- email envoye -------")
        dispatcher.utter_message("Thanks for providing the details. We have sent you a mail at {}".format(tracker.get_slot("email")))
        return []

def SendEmail(toaddr,subject,message):

    fromaddr = "rasatest100@gmail.com"
    msg = MIMEMultipart()
    msg['From'] = fromaddr
    msg['Subject'] = subject
    body = message

    msg.attach(MIMEText(body, 'plain'))
    s = smtplib.SMTP('smtp.gmail.com', 587)

    s.starttls()

    try:
        s.login(fromaddr, "rasa//test100")

        text = msg.as_string()

        s.sendmail(fromaddr, toaddr, text)

    except:
        print("An Error occured while sending email.")
    finally:
        # terminating the session
        s.quit()



	
