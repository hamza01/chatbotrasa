## Hamza Harbouch & Achraf Zakraf

# Projet NAO-Rasa 2021
Contact : 
hamza.harbouch@alumni.univ-avignon.fr
achraf.zakraf@alumni.univ-avignon.fr

## Installation

**Attention** : dans tout les fichiers il faut utiliser la vraie IP de vos servers dans les fichiers de config

### Serveur de dialogue

- Lancer sur le racine de dossier de projet : rasa run actions

- Lancer sur le racine de dossier de projet : rasa run --enable-api

==============

### Serveur de reconnaissance de parole
- le serveur de google speech est dans le dossier projet_monrobotparlant
- pip install speechRecognition
- Serveur : py googleSR_serveur.py

==============

### NAO :

- Démarrer Chorégraphe
- Ouvrir Projet NaoRasaProject/NaoRasa.pml
- Modifier boites RasaCall et SpeechRecognition pour ajuster les IP des serveurs
- Connecter au NAO (soit filaire, soit wifi)
- Charger le projet sur le robot (F5)
- Tester

==============

- Premier Api : disponibilité de salle

![](images/1.png)

- Deuxieme Api : Cherche la matiere de l'ensignent

![](images/2.png)

- Troisieme Api : l'envoie des emails

![](images/3.png)

Demo : 

![](images/demoemail.png)















